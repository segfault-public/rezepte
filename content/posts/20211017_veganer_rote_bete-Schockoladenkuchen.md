---
title: "Veganer Rote Bete Schockoladenkuchen"
date: 2021-10-17T05:45:00+02:00
draft: false
tags: [vegan, Kuchen]
categories: [Rezepte]
---

# Zutaten für den Kuchen

| Menge | Zutat |
| --- | --- |
| 130 Gramm | Rohrzucker oder brauner Zucker bzw. Xylit |
| 220 Gramm | Weizenmehl Type 550 |
| 30 Gramm | Backkakao |
| 2 Teelöffel | Backpulver |
| 1/2 | Teelöffel Natron |
| 1/2 | Teelöffel Vanilleextrakt |
| 1 | Prise Salz |
| 100 Milliliter | Hafermilch oder Sojamilch |
| 150 Milliliter | Sonnenblumenöl |
| 1 Teelöffel | Zitronensaft |
| 1 Teelöffel | Apfelessig |
| 200 Gramm | Rote Bete vorgegart und vakuumiert |

# Zutaten für den Schockoladenguss

| Menge | Zutat |
| --- | --- |
| 150 Gramm | dunke Schockolade |
| 60 ml | Sonnenblumenöl |

# Anleitung
- Die Rote Bete klein schneiden und mit dem Zucker pürieren, sodass keine Stückchen mehr vorhanden sind.
- Backofen auf 175 Grad Ober- und Unterhitze vorheizen. Eine kleine Springform fetten und mit etwas Kakao bestäuben.
- Mehl, Kakao, Backpulver, Natron, Salz in einer Schüssel mischen. Pflanzliche Milch, Öl, Apfelessig, Zitronensaft und Vanilleextrakt verquirlen und zum Rote-Bete-Mus geben.
- Die feuchten Zutaten zu den trockenen geben und alles kurz, aber kräftig, zu einem homogenen Teig verrühren.
- In die vorbereitete Form füllen und glattstreichen.
- Ca. 35-40 Minuten backen. Evtl. Stäbchenprobe machen, der Kuchen darf aber innen noch etwas feucht sein.
- Wenn der Kuchen ausgekühlt ist, die Schockolade mit den 60ml Sonnenblumenöl im Wasserbad schmelzen und den Kuchen damit überziehen.

Quelle:
[Backen macht glücklich - Supersaftiger veganer Rote-Bete-Schockoladenkuchen](https://www.backenmachtgluecklich.de/rezepte/saftiger-veganer-rote-bete-schokoladenkuchen.html)
