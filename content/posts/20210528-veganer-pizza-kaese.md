---
title: "Veganer Pizzakäseersatz"
date: 2021-05-28T13:51:16+02:00
draft: false
tags: [Pizza, vegan]
categories: [Rezepte]
---

| Menge | Zutaten |
| --- | --- |
| 40 g | Cashewkerne |
| 350 ml | Wasser |
| 2 - 3 EL | Tapiokastärke oder Tapiokamehl |
| 2 EL | Hefeflocken |
| 1 TL | Salz |
| 1 EL | Agar-Agar |
| 1 TL | Limetten- oder Zitronensaft |
| 1 TL | Agavendicksaft |
| 80 ml | Pflanzenöl |



1. Die Cashewkerne in das Wasser über Nacht einweichen oder das Wasser aufkochen und zusammen mit dem Cashew Kernen für 30 Minuten stehen lassen.
1. Alle flüssigen Zutaten in den Mixbehalter geben
1. Danach alle festen Zutaten in den Mixbehalter
1. Mixen bis eine homogene Flüssigkeit entsteht
1. Diese in einem Topf unter ständigem Rühren 1 Minute aufköcheln lassen
1. Die Masse sollte nun fester geworden sein und leichte Fäden ziehen
1. Die Masse nun in eine Schüssel umfüllen und für mindestens 1 Stunde im Kühlschrank auskühlen lassen
1. Nun den Käse mit einem Löffel auf der Pizza verteilen

Agar-Agar haben wir im Reformhaus Merk gekauft.

Tapiokamehl bekommt man beim Asiaten.

[Quelle: Youtube - Wie du den besten veganen Pizzakäse selbst machst! von Bananenbrot TV](https://www.youtube.com/watch?v=A48jpquPu2Q)
