---
title: "Schneller veganer Mohnkuchen"
date: 2021-09-11T05:45:00+02:00
draft: false
tags: [vegan, Kuchen]
categories: [Rezepte]
---

| Menge | Zutat|
| --- | --- |
| 100 g | Mohn gemahlen |
| 200 g | Weizenmehl |
| 80 g | Rohrohrzucker |
| 200 g | Kokusnussjoghurt |
| 100 ml | Backöl |
| 30 ml | Rum oder Pflanzenmilch |
| 2 TL | Backpulver |
| 1/4 TL Bourbon Vanile gemahlen |

# Anleitung

1. Backofen auf 180°C Umluft vorheizen
1. Springform mit 20cm Duchmesser mit Backpapier auslegen und etwas mit Öl einstreichen
1. Alle Zutaten mit der Küchenmaschine gut verrühren bis der Teig leicht flüssig ist
1. Den Teig in die Form geben und glattstreichen
1. In den Ofen geben und 25-30 Minuten backen
1. Nachdem der Kuchen komplett ausgekühlt ist, vorsichtig aus der Backform lösen

Quelle:
- [veganvibes.de - Schneller Mohnkuchen](https://www.veganevibes.de/schneller-mohnkuchen-30-minuten/)
