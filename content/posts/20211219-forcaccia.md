---
title: "Focaccia"
date: 2021-12-19T13:51:15+02:00
draft: false
tags: [backen, vegan, Italien]
categories: [Rezepte]
---


| Menge | Zutat |
| --- | --- |
| 500 g | Mehl |
| 7 g | Trockenhefe |
| 325 ml | lauwarmes Wasser |
| eine Prise | Salz |
| 1-2 TL | Ölivenöl |
| etwas | Hartweizengrieß oder Polenta |

1. Wasser und Hefe miteinander vermischen
1. Alle Zutaten bis auf das Grieß/Polenta zusammengeben
1. Den Teig 10 Minuten kneten
1. Den Teig bedeckt 40 Minuten (oder übernacht im Kühlschrank) ruhen lassen
1. Den Grieß/Polenta in eine Auflaufform verteilen
1. Den Teig in die Auflaufform geben
1. Auf den Teig ca 1GL Ölivenöl geben
1. Forcaccio nach belieben belegen (z.B. Tomaten, Pilze, Kräuter,...)
1. Etwas Salz und Pfeffer dazu geben
1. Die Auflaufform mit einem feuchten Küchentuch abdecken und für 45 Minuten ruhen lassen
1. 25 Minuten bei 200°C im vorgeheizten Ofen backen


Tipp: Kräuter geben ihren Geschmak besonders gut ab, wenn man es vorher mit Öl eingerieben hat.



Quelle: [youtube.com - How to make focaccia - Jamie & Gennaro](https://www.youtube.com/watch?v=UX6K-Z67vI4)
