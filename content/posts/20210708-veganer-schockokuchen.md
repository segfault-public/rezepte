---
title: "Veganer Schocko-Bananenkuchen"
date: 2021-07-08T13:51:16+02:00
draft: false
tags: [vegan, Kuchen, backen]
categories: [Rezepte]
---




# Zutaten für eine Auflaufform (4 Portionen)

| Menge | Zutat|
| --- | --- |
| 50 g | Kokosöl (alternativ Butter) | 
| 2 | reife Bananen | 
| 2 Eier | (vegan: 2 EL Chiasamen mit 6 EL Wasser verrühren, „Chia-Ei“ 10 Minuten quellen lassen oder 2 EL Leinsahmen mit 6 EL kochendem Wasser verrrühren und "Leinsamen-Ei" 10 Minuten quellen lassen) | 
| 70 g | Kokosblütenzucker (alternativ Vollrohrzucker) | 
| 100 ml | Pflanzenmilch | 
| 30 g | Kakao | 
| 140 g | gemahlene Mandeln | 
| 100 g | Vollkornmehl (bei mir: Buchweizenmehl) | 
| 2 TL | Backpulver | 
| 1 Prise | Salz | 
| 50 g | dunkle Schokolade (mind. 60% Kakao) | 

# Glasur:
| Menge | Zutat|
| --- | --- |
| 50 g | dunkle Schokolade (mind. 60% Kakao) | 
| 1 EL | Erdnuss-Mus | 













# Anleitung

1. Backofen auf 180 Grad Ober- und Unterhitze vorheizen. Eine eckige Kastenform von ca. 23 cm leicht fetten und mit Backpapier auslegen.
1. Kokosöl in einer kleinen Pfanne schmelzen und leicht abkühlen lassen. Zusammen mit den Bananen, Eiern, Kokosblütenzucker, Milch und Kakao im Mixer oder mit einem Pürierstab cremig pürien.
1. Gemahlene Mandeln, Mehl, Backpulver und Salz in eine Schüssel geben und mischen. Die flüssigen Zutaten beigeben und kurz unterheben. Schokolade fein hacken und unter den Teig heben.
1. Teig in die vorbereitete Backform füllen und bei 180 Grad Ober- und Unterhitze für 40 Minuten backen. Vor dem Herausnehmen die Stäbchenprobe machen.
1. Für die Glasur die Schokolade fein hacken und über dem Wasserbad schmelzen. Erdnussmus dazugeben und unterrühren. Die Glasur auf dem abgekühlten Kuchen verteilen, nach Belieben mit gehobelten Mandeln und getrockneten Cranberries garnieren.

Quelle:
- Video: [Youtube - Mrs Flury - Gesunder Schocko-Bananenkuchen](https://www.youtube.com/watch?v=Nz8rHuYPQN0)
- Blog: [Mrs Flury Block - Gesunder Schocko-Bananenkuchen](https://www.mrsflury.com/schoko-bananenkuchen-glutenfrei-ohne-weissen-zucker/)
