---
title: "Vegane Burger Patties"
date: 2021-07-20T05:45:00+02:00
draft: false
tags: [vegan, kochen, Burger]
categories: [Rezepte]
---

# Zutaten für 2 Portionen

| Menge | Zutat|
| --- | --- |
| 150 g | Champignons |
| 50 | Weizenmehl |
| etwas| Worcester Sauce |
| etwas | Paprikapulver |
| 2 EL | Wasser |
| | Salz |
| | Pfeffer |
| | Öl |

# Anleitung

1. Champignons grob raspeln.
1. Mit dem Rest der Zutaten gründlich vermengen und abschmecken.
1. In einer Pfanne auf mittlerer Stufe etwas Öl erhitzen. Masse als Häufchen hineingeben und etwas platt drücken. Ca. 3 Minuten pro Seite anbraten.

Quelle:
- Hello Fresh