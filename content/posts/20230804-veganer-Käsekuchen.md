---
title: "Veganer Käsekuchen"
date: 2023-08-04T13:51:15+02:00
draft: false
tags: [backen, kuchen]
categories: [Rezepte]
---


# Boden
| Menge | Zutat |
| --- | --- |
| 300 g | Mehl (Weizenmehl 405, Weizenmehl 550 oder Dinkelmehl 630) |
| 150 g | Zucker | 
| 180 g | Magarine | 
| 1 Packung | Vanillezucker | 
| 1/2 Packung | Backpulver | 
| 1/4 Teelöffel | Salz | 

# Füllung:
| Menge | Zutat |
| --- | --- |
| 2 Becher | Alpro Skyr Style Vanille (2 x 400g) |
| 200 g | Zucker |
| 100 g | Magarine |
| 30 g | Zitronensaft |
| 1/2 - 1 Tüte | geriebene Zitronenschale |

# Boden:

1. Eine 26 cm Springform einfetten.
1. In einer Schüssel mischen:
    - 300 g Mehl
    - 150 g Zucker
    - 1 Packung Vanillezucker (8 g)
    - ½ Packung Backpulver (9 g)
    - ¼ TL Salz (2 g)
1. 180 g vegane Butter zufügen.
1. Alles zu einem Teig verkneten.
1. Den Teig in der gefetteten Springform verteilen.
1. Festdrücken.
1. Dabei einen Rand formen (ca. 2,5 cm hoch).
1. Jetzt den Ofen vorheizen: 180 °C (Ober-Unterhitze)

# Füllung:

1. 100 g vegane Butter schmelzen. (Nicht zu heiß.)
1. Zitrone auspressen.
1. In einer Schüssel mischen:
    - 2 Becher Alpro Skyr Vanille (2 x 400 g)
    - 200 g Zucker
    - 30 g Zitronensaft
    - ½ bis 1 Tüte abgeriebene Zitronenschale
    - ½ bis 1 Fläschchen Zitronenaroma
    - 100 g geschmolzene vegane Butter
1. Mit den Rührhaken eines Mixers gut verrühren.
1. Dann 2 Packungen Vanillepuddingpulver zufügen (2 x 37 g). Es soll eine schöne glatte Masse ohne Klümpchen sein.
1. Die Füllung auf den Boden in der Springform gießen.

# Backen:

1. Im vorgeheizten Ofen bei 180 °C (Ober-Unterhitze) auf der mittleren Schiene 70 Minuten backen.
1. Nach dem Backen im geöffneten Backofen vollständig auskühlen lassen.
1. Den Kuchen erst aus der Springform nehmen, wenn er völlig kalt ist.

Quelle: [Veggie Einhorn - Veganer Käsekuchen](https://veggie-einhorn.de/bester-veganer-kaesekuchen/)
