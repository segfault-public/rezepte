---
title: "Burger Buns"
date: 2021-12-24T13:51:15+02:00
draft: false
tags: [Burger]
categories: [Rezepte]
---


| Menge | Zutat |
| --- | --- |
| 500g | Mehl |
| 250ml | Wasser oder Milch |
| 1Pkg | Hefe (oder 20g frisch) |
| 3/4 - 1 | EL Salz |
| 2 EL | Zucker |
| 1 | Ei |
| 2 EL | Butter (weich) |

1. Das Wasser mit der Hefe, Salz, Zucker Ei und Butter zu einer gleichmäßigen Masse verrühren
1. Nun das Mehl hinzugeben
1. Den Teig für mehrere Minuten kneten
1. Den Teig für 1-2 Stunden gehen lassen bis sich der Teig verdoppelt hat
1. Den Teig in 10 gleich große Teile aufteilen, zu Kugeln formen, platt drücken und auf ein Blech verteilen
1. Die Rohlinge nun für 30-60 Minuten noch einmal gehen lassen+
1. Im vorgeheizten Ofen bei 180°C für ca. 15 Minuten backen




Quelle: [youtube.com - kein Stress kochen - Burger für 1€](https://www.youtube.com/watch?v=6Dd2CR93Klg)
