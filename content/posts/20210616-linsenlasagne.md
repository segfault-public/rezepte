---
title: "Linsenlasagne"
date: 2021-06-16T13:51:16+02:00
draft: false
tags: [vegan]
categories: [Rezepte]
---




# Zutaten für eine Auflaufform (4 Portionen)

| Menge | Zutat|
| --- | --- |
| 2 | Zwiebeln |
| 2 EL | Öl zum Anbraten, z.B. Rapsöl |
| 120 g | grüne Linsen |
| 300 ml | Gemüsebrühe/Wasser |
| | Gemüse der Saison, z.B. 1 Zucchini, 2 Karotten, 4 Champignons, 3 Tomaten |
| 50 g | Walnüsse |
| 25 g | Haferflocken |
| 1,5 TL | Sambal Oelek |
| 1,5 TL | Senf |
| 1 Schuss | Agavendicksaft oder Dattelsirup (siehe https://www.youtube.com/watch?v=iPgKV...) |
| 2-3 EL | Tomatenmark |
| | Kräutermix, z.B. Basilikum, Oregano, Thymian |
| | Salz & Pfeffer |
| | Vollkorn-Lasagneplatten |

# Zutaten für das Topping

| Menge | Zutat|
| --- | --- |
| 2 EL | Cashewmus |
| 2 EL | Hefeflocken |
| 1 TL | Zitronensaft |
| 3-4 EL | Wasser |
| 1 | Knoblauchzehe (optional) |

# Anleitung

1. Die Zwiebeln würfeln, das Öl in einer großen Pfanne erhitzen und die Zwiebeln darin goldbraun anbraten
1. Die Linsen gründlich spülen und anschließend zu den Zwiebeln in die Pfanne geben
1. Mit der Gemüsebrühe ablöschen
1. Einmal mit geschlossenem Deckel aufkochen, dann die Temperatur reduzieren und nur noch leicht köcheln lassen
1. In der Zwischenzeit das Gemüse waschen und in kleine Würfel schneiden
1. Nach gut 10 Minuten zu den Linsen geben weiterköcheln lassen, bis die Linsen gar sind. Bei Bedarf noch etwas Wasser hinzugeben.
1. Walnüsse und Haferflocken in einem kleinen Mixer zu einer mehlähnlichen Konsistenz verarbeiten
1. Unter das Linsengemüse rühren
1. Mit Sambal Oelek, Senf, Agavendicksaft, Tomatenmark, Kräutern, Salz und Pfeffer abschmecken
1. Die Soße im Wechsel mit den Lasagneplatten in eine Auflaufform schichte, dabei mit einer Schicht Soße starten und enden
1. Cashewmus, Hefeflocken, Zitronensaft, Wasser und fein geschnittenen Knoblauch (optional) in einem Glas verrühren und als Topping auf die Lasagne streichen
1. Bei 180 °C für ca. 20-25 Minuten im Ofen backen, bis die Lasagne eine leckere braune Kruste bekommen hat

Quelle: [Youtube - User Favorit in der Krise: LINSEN-LASAGNE von Lecker Lecker Vegan](https://www.youtube.com/watch?v=YbNkLCgE0Ko)
