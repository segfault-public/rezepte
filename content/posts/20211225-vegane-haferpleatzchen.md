---
title: "Vegane Haferplätzchen"
date: 2021-12-25T13:51:15+02:00
draft: false
tags: [backen, vegan, süß, glutenfrei]
categories: [Rezepte]
---


| Menge | Zutat |
| --- | --- |
| 100 g | glutenfreie Haferflocken |
| 20 g | gemahlene Haselnüsse oder Mandeln |
| ½ TL | Backpulver |
| 1 Prise | Salz |
| Geriebene Schale 1 | Bio-Orange |
| 30 g | Zucker |
| 30 g | Rohrzucker |
| 10 g | Vanillezucker |
| Optional 1 ½ TL | Lebkuchengewürz* |
| 3 EL (= 45 ml) | Hafermilch (oder eine andere Pflanzenmilch eurer Wahl - Achtung, Dinkelmilch enthält Gluten! Alternativ auch einfach Wasser) |
| 4 EL (=60 ml) | geschmacksneutrales Öl (ich nehme am liebsten das Bio Brat- & Backöl) |

1. Haferflocken im Mixer zu Mehl mahlen
1. Alle Zutaten vermengen bis eine gleichmäßige Masse entsteht
1. Den Teig in ca. 3mc große Kugeln formen und auf dem Backbleck flach drücken
1. Im Ofen für ca. 12 Minuten bei 160°C goldbraun backen.
1. Kurz auf dem Bleck abkühlen lassen, dann vorstitig auf ein Rost zum Abkühlen setzen.

Tipp: Der Teig wird sehr schnell zäh, daher sollte er möglichst schnell verarbeitet werden.

Tipp: Die Kekse schmecken mit einer Schockoglasur noch besser

Quelle: [caceinvasion.de - Haferplätzchen Vegan Glutenfrei](https://cakeinvasion.de/2015/12/haferplaetzchen-vegan-glutenfrei/)
