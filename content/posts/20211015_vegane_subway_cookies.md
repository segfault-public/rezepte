---
title: "Vegane Subway Cookies"
date: 2021-10-16T05:45:00+02:00
draft: false
tags: [vegan, Cookies, backen]
categories: [Rezepte]
---

# Zutaten

| Menge | Zutat |
| --- | --- |
| 280 g | Mehl |
| 250 g | Butter |
| 100 g | Zucker, braun |
| 130 g | Zucker |
| 2 EL | Chiasamen |
| 6 EL | Wasser |
| 1 TL | Natron |
| ½ TL | Salz |
| 1 Packung | Vanillezucker |
| 200 g | Schokolade nach Wahl, grob gehackt oder Nüsse |

# Anleitung
Die Teigmenge reicht für ca. 20 Cookies.

Die Butter mit beiden Zuckersorten und dem Salz schaumig schlagen. Die Eier und den Vanillezucker zufügen und cremig rühren. Etwas Mehl beiseite nehmen und mit dem Natron mischen, das restliche Mehl löffelweise unter die Masse rühren, zum Schluss das Mehl-Natron-Gemisch einrühren. Der Teig ist vergleichsweise fest und zäh. Schokolade, Nüsse etc. unter den Teig mischen.

Den Teig esslöffelweise auf ein mit Backpapier ausgelegtes Backblech setzen. Viel Platz lassen! Die Cookies zerlaufen sehr stark. Maximal 8 Cookies pro Blech.

Bei Umluft 190 °C ca. 8 - 9 Minuten backen. Wichtig: Je nach Ofen sind die Cookies etwas früher oder später fertig. Perfekt sind sie, wenn die Ränder ganz leicht braun sind und die Cookies im Kern noch feucht wirken.

Dann sofort aus dem Ofen nehmen und ganz vorsichtig, sie sind sehr zerbrechlich, auf eine Platte o. ä. zum Auskühlen setzen. Nicht auf dem heißen Blech lassen! Beim Auskühlen härten sie dann nach, bleiben innen aber schön zart. Sind sie knusprig geworden, waren sie zu lange im Ofen! Am besten gekühlt in einer Dose aufbewahren. Sie halten ca. 4 Tage.

# Cookie-Variationen:

Macadamia-White-Chocolate-Cookies: 200 g grob gehackte weiße Schokolade und 150 g gehackte Macadamianüsse unter den Teig geben. Gesalzene Nüsse vorher gut abspülen.

Double-Chocolate-Cookies: 150 g gehackte dunkle Schokolade, 1 EL dunklen Kakao unter den Teig geben.

Chocolate-Walnut-Cookies: 150 g grob gehackte dunkle Schokolade, 100 g grob gehackte Walnüsse unter den Teig geben. 

Quelle:
[Chefkoch.de - American Cookies wie bei Subway](https://www.chefkoch.de/rezepte/1378031243002711/American-Cookies-wie-bei-Subway.html)
