---
title: "Veganer Russicher Zupfkuchen"
date: 2021-09-11T05:45:00+02:00
draft: false
tags: [vegan, Kuchen]
categories: [Rezepte]
---

# Zutaten

| Menge | Zutat |
| --- | --- |
| 1 Prise | Salz |
| 2 Packungen | Puddingpulver |
| 230 g | vegane und weiche Margarine |
| ca. 50 ml | Soja-Drink |
| 300 g | Himbeeren |
| 1 kg | Sojajoghurt |
| 1 geh. TL | Backpulver |
| 200 g | Zucker |
| 200 g | Mehl |
| 3 gehäufte EL | Kakaopulver |
| ½ unbehandelte | Zitrone |

# Anleitung

1. Zuerst gibst du das Mehl, 100 g Zucker, 130 g Margarine, Backpulver, Salz, Kakaopulver und den Soja-Drink in in eine Schüssel und vermengst alles mit dem Handrührgerät. Möglicherweise musst du noch etwas mehr von dem Soja-Drink hinzugeben, falls der Teig zu trocken ist.
1. Nun kannst du eine Springform mit Backpapier auslegen. 3/4 des Teiges wird zu einem Boden in die Springform gedrückt und dabei ziehst du einen 5 cm Rand hoch. Den Boden für ca. 30 Minuten in den Kühlschrank stellen.
1. Dann kannst du den Backofen auf 180 Grad Ober-/Unterhitze vorheizen. Für die Füllung die restliche Margarine mit dem übrigen Zucker verrühren. Jetzt den Joghurt, das Puddingpulver und etwas Zitronenabrieb unterrühren. Die Himbeeren auf dem Schokoboden in der Springform verteilen und mit der Puddingmasse übergießen. Nun den Rest des Schokoteiges in Flecken auf der Puddingmasse verteilen und alles im heißen Ofen für ca. 45-50 Minuten backen. Den Kuchen vor dem Anschneiden gut abkühlen lassen.

Quelle:
- [yumtamtam.de - Vegan backen: Russischer Zupfkuchen](https://yumtamtam.de/Rezepte/Veganer-Zupfkuchen.html)
