---
title: "Vegane Blizmayonnaise"
date: 2021-11-06T13:51:15+02:00
draft: false
tags: [Burger, vegan]
categories: [Rezepte]
---


| Menge | Zutat |
| --- | --- |
| ca. 250 ml | Grukenwasser (ein Glas Gewürzgurken ohne Gurken) |
| 250g | Cashewmus |

1. Zusaten zusammengeben
1. Vermischen bis eine gleichmäßige Masse entstanden ist

Quell: [smarticular.net - Vegane Blizmayonnaise](https://www.smarticular.net/blitzmayonnaise-vegan-cashew-mayonnaise-gurkenwasser-rezept/)
