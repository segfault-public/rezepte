---
title: "Dinkel-Baguette"
date: 2021-11-28T13:51:15+02:00
draft: false
tags: [backen, vegan]
categories: [Rezepte]
---


| Menge | Zutat |
| --- | --- |
| 350 ml | Wasser |
| 1/2 | Hefewürfel | 
| 1 Prise | Zucker | 
| 1 TL | Salz | 
| 500 g | Dinkelmehl | 
| 1 EL | Olivenöl | 

1. Löse die Hefe im warmen Wasser auf.
1. Gib den Zucker, das Salz und das Dinkelmehl hinzu.
1. Verrühre alles mit dem Handrührgerät zu einem glatten Teig.
1. Gib das Olivenöl hinzu und verrühre die Zutaten erneut.
1. Knete nun die Masse mit den Händen zu einem weichen Teig.
1. Decke die Schüssel mit dem Teig mit einem Küchentuch ab.
1. Lasse den Teig etwa eine Stunde bei Raumtemperatur gehen.
1. Bemehle deine Arbeitsfläche.
1. Forme aus dem Teig drei Baguettes.
1. Schneide jedes Baguette mehrmals mit einem Messer ein.
1. Gib die Baguettes auf ein mit Backpapier ausgelegtes Backblech.
1. Lasse die Baguettes noch einmal 30 Minuten im Ofen gehen.
1. Lasse die Baguettes etwa 20 bis 25 Minuten bei 200 Grad im Ofen goldbraun backen.

Quelle: [utopia.de - Dinkel-Baguette](https://utopia.de/ratgeber/dinkel-baguette-veganes-rezept-mit-dinkelmehl/)
