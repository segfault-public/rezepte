---
title: "Big Mac Sauce"
date: 2021-12-23T13:51:15+02:00
draft: false
tags: [Burger]
categories: [Rezepte]
---


| Menge | Zutat |
| --- | --- |
| 120 g | Mayonnaise |
| 4 TL | Gurken-Relish |
| 1 TL | Zwiebel (sehr fein gehackt) |
| 1/2 | Knoblauchzehe |
| 2 TL | Senf mittelscharf |
| 1 TL | Ketchup |
| 1 TL | Zucker |
| 1/2 TL | Paprikapulver edelsüß |
| 1/4 TL | Weißweinessig |
| 1 Msp | Salz |
| 1 Msp | gemahlener Pfeffer |

1. Zwiebel und Knoblauch sehr fein hacken
1. Alle Zutaten gut vermengen bis eine homogene Masse entsteht.
1. Optimalerweise die Soße über Nacht im Kühlschrank noch ziehen lassen.

Tipp: Wenn man die Mayonnaise durch die Blizmayonese ersetzt, ist die Soße sogar vegan.

Quelle: [bbgpit.de - Big Mac Sauce](https://bbqpit.de/big-mac-sauce)
