---
title: "Veganer Apfelkuchen"
date: 2022-01-07T13:51:15+02:00
draft: false
tags: [backen, vegan, Kuchen]
categories: [Rezepte]
---

### Zutaten Teig und Streusel
| Menge | Zutat |
| --- | --- |
| 1 | kleine Banane |
| 300g | Dinkelmehl |
| 180g | Margarine |
| 80g | Zucker |

### Zutaten Füllung
| Menge | Zutat |
| --- | --- |
| 6 | große Äpfel |
| 2 EL | Wasser |
| 2 EL | Zucker |
| 1 TL | Zimt |

### Anleitung
1. 26cm Springform einfetten
1. Banane mit Magerine und Zucker cremig rühren
1. Mehl hinzugeben und zu einem gleichmäßigen Teig verarbeiten
1. Mit dreiviertel des Teiges in der Springform verteilen
1. Springform und restlichen Teig in den Kühlschrank für eine halbe Stunde stellen
1. Äpfel schälen und in kleine Stücke schneiden
1. Äpfel mit dem Wasser, Zucker und Zimt beim mittlerer Hitze für ca. 10 Minuten köcheln
1. Topf ein wenig abkühlen lassen
1. Die Apfelmasse auf dem Kuchen verteilen und glatt streichen
1. Restlichen Teig als Streusel auf dem Kuchen verteilen
1. Kuchen im vorgeheizten Ofen bei 180°C Ober- und Unterhitze für ca. 35 Minuten backen

Quelle: [mrsflury.com - Apfel-Streusel-Kuche](https://www.mrsflury.com/apfel-streusel-kuchen/)
