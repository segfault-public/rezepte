---
title: "Brokkoli Patty"
date: 2021-12-05T13:51:15+02:00
draft: false
tags: [Burger, vegan]
categories: [Rezepte]
---


| Menge | Zutat |
| --- | --- |
| 250g | Brokkoli-Röschen |
| 1 EL | Öl |
| 60g | Kichererbsenmehl |
| 1 | Knoblauchzehe |
| 1 TL | Paprikapulver |
| | Salz und Pfeffer |


1. Bringe in einem Kochtopf Wasser zum Kochen und gib die Brokkoli-Röschen hinein. Lasse sie für ca. 4 Min. köcheln.
1. Seihe die Brokkoli-Röschen ab und zerdrücke sie mit einer Gabel und schäle und presse deine Knoblauchzehe.
1. Vermische in einer Schüssel die zerdrückten Brokkoli-Röschen, das Kichererbsenmehl, die Knoblauchzehe, das Paprikapulver, dem Öl sowie Salz + Pfeffer.
1. Lasse den Teig für etwa 10 Min. ziehen.
1. Erhitze ein wenig Öl in einer Pfanne und forme aus dem Brokkoli-Teig 4 Patties.
1. Brate die Patties auf jeder Seite für ca. 4-5 Min.


Quelle: [kitchenfea.com - Brokkoli Patty Burger](https://kitchenfae.com/brokkoli-patty-burgers/)
