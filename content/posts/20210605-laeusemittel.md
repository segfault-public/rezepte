---
title: "Hausmittel gegen Lausbefall von Pflanzen"
date: 2021-06-05T11:11:16+02:00
draft: false
tags: [Pflanzen, Läuse]
categories: [Pflanzen]
---

| Menge | Zutaten |
| --- | --- |
| 1 l | Wasser |
| 1 EL | Öl |
| 1 EL | Backpulver |

1. Alles gut vermischen und in eine Sprühflasche geben
1. Die Pflanzen an den betroffenen stellen damit täglich einsprühen
