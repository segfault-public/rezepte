---
title: "Sommerrrollen"
date: 2022-01-21T13:51:15+02:00
draft: false
tags: [kochen, asiatisch]
categories: [Rezepte]
---
Für ca. 20 Stück.

### Zutaten Frühlingsrollen
| Menge | Zutat |
| --- | --- |
| 20 |  Reispapierblätter |
| 250 g | Reisnudeln |
| 1 | kleine Gurke |
| 1 | kleiner Eisbergsalat |
| 3 | junge Möhren |
| 150 g | Hühnerfleisch oder Schweinefleisch (optional) |
| 30 Stück |  Garnelen (optional) |
| 1 Packung | Tofu (optional) |
| 4 | Eier |
| 1 | Bund Minze  |
| | Salz, Pfeffer |
| | Öl zum Eier braten|

### Zutaten Soße
| Zutat |
| --- |
| Hoisin-Soße |
| Erdnussmus (alternativ: Cashewmus) |
| Wasser |
| Geröstete Erdnussstückchen (optional) |

### Zubereitung 
Reisnudeln in viel Wasser kochen, rausholen, mit kaltem Wasser abschrecken und abtropfen.  
Fleisch in leicht gesalzenem Wasser kochen, danach in dünne Streifen schneiden. 

Garnelen kurz garen, schälen, in der Mitte vom Rücken in 2 dünnen Hälfte schneiden. 
Tofu in längliche Streifen schneiden und leicht knusprig anbraten.  

Eier schlagen, mit bisschen Salz und Pfeffer gut durchmischen, auf der Pfanne dünn (wie Pfannkuchen) 
braten, danach in feinen Streifen schneiden. 

Gurke und Salat in dünnen 4-5 cm lang Streifen schneiden.  

Möhren putzen, schälen und in sehr feine Streifen 4-5 cm lang schneiden. Minzen putzen. 

Die angefeuchteten Reispapierblätter auf einem Brett oder Teller ausbreiten und jedes mit den 
vorbereiteten Zutaten belegen, am besten die Reihenfolge: Reisnudel, Salat, Möhren, Gurke, (Fleisch, 
Tofu, Garnelen) und Minzen-Blätter. In jeder Rolle gibt es 3 Garnelen-Hälften, die schön in eine Linie 
entlang der Rollenlänge vorne zu legen sind. Die Blätter aufrollen, dabei die Enden gut verschließen. 

Hoisin-Soße, Erdnussmus (alternativ Cashewmus) und Wasser gut durchmischen bis eine relativ 
dickflüssige Soße entsteht, in einem Topf kurz aufkochen. 

Sommerrollen mit Soße servieren. 
