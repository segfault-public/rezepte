---
title: "Vanillekipferl vegan"
date: 2021-12-26T13:51:15+02:00
draft: false
tags: [backen, vegan, süß]
categories: [Rezepte]
---

Rezept für ca. 100 Kipferl

### Zutaten für den Teig
| Menge | Zutat |
| --- | --- |
| 300 g | Mehl |
| 1 EL | Stärke |
| 150 g | gemahlene Mandeln, geschält oder auch ungeschält |
| 80 g | Puderzucker |
| 1/4 TL | Salz |
| 230 g | rein pflanzliche Margarine (zimmerwarm für Handrührer oder Küchenmaschine, kalt für die Verarbeitung per Hand), für eine sojafreie Version darauf achten, dass die Margarine frei von Sojabestandteilen ist |
| 1  |Vanilleschote |

### Zutaten für die Zuckerkruste
| Menge | Zutat |
| --- | --- |
| 50 g | Zucker |
| 3 EL |Vanillezucker |
| 30 g | Puderzucker |

1. Die Vanilleshote längs einschneiden, auseinanderklappen und mti der Rückseite des Messer das Mark auskratzen
1. Das Mark zusammen mit allen Zutaten für den Teig zusammen in eine Schüssel geben und gutvermengen
1. Den Mürbteig in 4 gleich große Stücke aufteilen
1. Auf der mit Mehl bestäubten Arbeitsfläche jedes Teil in ca 1.5 cm dicke Rollen rollen
1. Die Rollen in Firschaltefolie packen und im Kühlschrank für mindestens 30 Minuten ruhen lassen
1. Die Rollen in ca. 2 cm große Stücke schneiden
1. Die Stücke zu Kipferl formen und auf ein Backblech geben
1. Im Ofen bei 175°C Ober/Unterhitze für ca 12-15 Minuten backen und herausholen, bevor sie goldbraun sind.
1. Nun den Zucker für die Zuckerkruste in einer kleinen Schüssel vermengen
1. Die warmen Vanillekipferl direkt nach dem Herausholen aus dem Ofen in der Zuckermischung wenden und auskühlen lassen



Quelle: [cakeinvastion.de - Vanillekipferl (Vegan)](https://cakeinvasion.de/2012/12/vanillekipferl-vegan/)
