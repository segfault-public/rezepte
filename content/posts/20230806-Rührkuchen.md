---
title: "Rührkuchen"
date: 2023-08-06T13:51:15+02:00
draft: false
tags: [backen, kuchen]
categories: [Rezepte]
---


| Menge | Zutat |
| --- | --- |
| 250 g | Zucker |
| 250 g | Butter | 
| 4 | Eier | 
| 0.1 l | Milch | 
| 250 g | Mehl | 
| 125 g | Mondamin Stärke | 
| 1 | Backpulver |

1. Zucker und Butter schaumig rühren
1. Eier und Milch langsam zugeben
1. Mehl, Mondamin und Backpulver sieben und dazugeben
1. Bei 160°C Heißluft für 45-50 Minuten backen

Anmerkung: Alle Zutaten müssen Raumtemperatur haben.

Quelle: Nachbar Scheurer
