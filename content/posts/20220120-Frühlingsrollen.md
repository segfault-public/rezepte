---
title: "Frühlingsrollen"
date: 2022-01-20T13:51:15+02:00
draft: false
tags: [kochen, asiatisch]
categories: [Rezepte]
---
Für ca. 20 Stück.

### Zutaten Frühlingsrollen
| Menge | Zutat |
| --- | --- |
| 20 | Frühlingsrollenblätter (Reispapier) |
| 25 g | Glasnudeln |
| 3-5 | getrocknete Mu-Err-Pilze |
| 4-5 | getrocknete Shiitake-Pilze |
| 2- 3 | junge Möhren |
| eine Handvoll |  Sojasprossen  |
| ein halber | Spitzkohl oder ein Kohlrabi |
| 150 g | Schweinehackfleisch (optional) |
| 3-4 | Eier |
| 1 | Zwiebel |
| Eine halber bis 1 | Bund Schnittlauch |
| | Salz, Pfeffer |
| | Öl zum Frittieren |

### Zutaten Soße
| Zutat |
| --- |
| Fischsoße oder Sojasoße |
| Essig oder Saft von Limetten/Zitronen  |
| Salz |
| Zucker |
| Knoblauch |
| Pfeffer oder Chili |
| Wasser |

### Zubereitung 
Glasnudeln und Pilze getrennt in Leitungswasser etwa 30 Min. quellen lassen, bis sie geschmeidig sind. 
Beides waschen und abtropfen lassen. Die Stiele von Mu-Err-Pilzen entfernen. Danach beide Pilze in sehr 
feine Streifen schneiden. Glasnudeln in ca. 4 cm lange Stücke schneiden. 

Spitzkohl putzen und in feine Streifen (Länge ca. 4 cm) schneiden. Möhren und evtl. Kohlrabi schälen, 
putzen und auch in feine Streifen (Länge ca. 4 cm) schneiden. Zwiebel putzen und in feine Scheiben 
schneiden. Schnittlauch waschen und in 0,5 cm lange Stücke schneiden. 

Glasnudeln, Pilze, Möhren, Sojasprossen, Spitzkohl, evtl. Kohlrabi, evtl. Hackfleisch, Eier, Zwiebel und 
Schnittlauch zusammen mischen. Die Mischung mit Salz und Pfeffer abschmecken. 

Die angefeuchteten Teigblätter auf einem Brett oder Teller ausbreiten und jedes mit etwas Füllung 
belegen. Die Blätter aufrollen, dabei die Enden gut verschließen. 

Das Öl zum Frittieren gut erhitzen. Die Frühlingsrollen darin in drei Portionen in je 3-4 Min. knusprig und 
goldgelb frittieren. Abtropfen und auf Küchenpapier ab-fetten lassen.  

Fischsoße oder Sojasoße verdünnt mit Wasser, danach mit Salz, Zucker und Essig oder Saft von 
Limetten/Zitronen abschmecken. Knoblauch sehr fein gehackt dazu geben. Die Soße mit Pfeffer oder 
frischem Chili-Stückchen verfeinern. Ziel ist eine dünne süßsaure Soße zum Eintunken. 

Warme Frühlingsrolle mit Soße servieren. 
